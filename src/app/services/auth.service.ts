import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {TokenResult} from "../models/token-result";
import {Router} from "@angular/router";
import jwtDecode from "jwt-decode";
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient, private router: Router) {
  }

  login(username: string, password: string): Observable<TokenResult> {
    return this.httpClient.post<TokenResult>(
      this.apiUrl + 'authentication_token', {
        username: username,
        password: password
      })
  }

  isLoggedIn(): boolean {
    if(localStorage.getItem("token")){
      let token = localStorage.getItem("token");
      if (typeof token === "string") {

        let decodedToken: any = jwtDecode(token);
        let currentTimeStamp = moment().format("X");

         if(decodedToken.exp && currentTimeStamp< decodedToken.exp){
           return true;
         } else {
           this.router.navigate(["login"]);
           return false;
         }
       }
      return  true;
    } else {
      this.router.navigate(["login"]);
      return false;
    }
  }

  logout(){
    localStorage.removeItem("token");
    this.router.navigate(["/login"]);
  }

  getToken(): string {
    return <string> localStorage.getItem("token");
  }
}

