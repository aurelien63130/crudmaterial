import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {Category} from "../../models/category";
import {CategoryService} from "../../services/category.service";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  displayedColumns: string[] = ['id', 'label', 'action'];
  dataSource?: Category[];

  constructor(private authService: AuthService,
              private categService: CategoryService) { }

  ngOnInit(): void {
    this.categService.getAll().subscribe(data => {
      this.dataSource = data;
    })
  }

  logout(){
    this.authService.logout();
  }

}
