import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from "@angular/router";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  hide = true;
  loginForm: FormGroup;
  apiError?: string;


  constructor(private fb: FormBuilder, private router: Router,
              private authService: AuthService) {
    this.loginForm = this.fb.group({
      username: ['',Validators.required],
      password: ['',Validators.required]
    });
  }



  getErrorMessage(input: string): string {
    if(input == 'username'){
        return 'Veuillez saisir un email';
    } else if (input == 'password'){
        return 'Veuillez saisir un mot de passe';
    } else {
      return 'Erreur inconnue';
    }
  }

  ngOnInit(): void {
  }

  login() {
    const val = this.loginForm.value;

    if(val.username && val.password){
      this.authService.login(val.username, val.password).subscribe(data => {
        if(data.token){
          localStorage.setItem("token", data.token)
        }
        this.router.navigate(["/"]);
      }, error => {
        this.apiError = error.error.message;
      });
    }
  }

  tooglePassword($event: any){
    if($event.pointerType !== ''){
      this.hide = !this.hide;
    }
  }
}
